# The Chromium Embedded Framework (CEF) project is built on top of the Chromium
# project source tree. Chromium should be updated to the URL and revision listed
# below before building CEF. Chromium compatibility information for older CEF
# revisions is available by viewing this file's change history.
#
# Instructions for building CEF are available at:
# https://bitbucket.org/chromiumembedded/cef/wiki/BranchesAndBuilding

{
  'chromium_checkout': '614d31daee2f61b0180df403a8ad43f20b9f6dd7',
}
